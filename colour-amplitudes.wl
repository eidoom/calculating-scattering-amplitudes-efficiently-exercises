(* ::Package:: *)

fourGluonScatteringOneLoop[a1_,a2_,a3_,a4_]:=
f[a1,a7,a6]f[a7,a2,a8]f[a5,a8,a3]f[a4,a6,a5]*D1+
f[a1,a7,a6]f[a7,a2,a8]f[a5,a8,a4]f[a3,a6,a5]*D2+
f[a1,a7,a6]f[a7,a3,a8]f[a5,a8,a2]f[a4,a6,a5]*D3;
SetAttributes[deltaI,Orderless]
SetAttributes[deltaA,Orderless]
traceOfThreeTs[a1_,a2_,a3_]:=Module[{i1,i2,i3},T[a1,i1,i2]T[a2,i2,i3]T[a3,i3,i1]];
colourContractOnce[expr_] := 
Return[
Expand[ 
expr /. {
f[a1_,a2_,a3_]:>-I/Sqrt[2](traceOfThreeTs[a1,a2,a3]-traceOfThreeTs[a1,a3,a2]),
T[a1_,i1_,i2_]T[a1_,i3_,i4_]:>deltaI[i1,i4]deltaI[i2,i3]-1/Nc deltaI[i1,i2] deltaI[i3,i4],
T[a1_,i1_,i2_] deltaI[i1_,i3_]:>T[a1,i3,i2],
T[a1_,i1_,i2_] deltaI[i2_,i3_]:>T[a1,i1,i3],
deltaI[i1_,i1_]:>Nc,
deltaI[i1_,i2_]deltaI[i2_,i3_]:>deltaI[i1,i3],
deltaI[i1_,i2_]^2->Nc,
T[a_,i_,i_]->0,
T[a1__,i1_,i2_]T[a2__,i2_,i3_]:>T[a1,a2,i1,i3],
T[a1__,i1_,i1_]:>trace[a1],
trace[a1_,a2_]:>deltaA[a1,a2]
}
]
];
Collect[FixedPoint[colourContractOnce,fourGluonScatteringOneLoop[a1,a2,a3,a4]],{deltaA[__],D1,D2,D3}]
