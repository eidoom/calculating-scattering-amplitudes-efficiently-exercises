(* ::Package:: *)

generateMasslessMomentum[range_:{-1,1}]:=Module[
	{p={RandomReal[range],RandomReal[range],RandomReal[range]}},
	Return[{Sqrt[Sum[p[[i]]^2,{i,3}]],p[[1]],p[[2]],p[[3]]}]
];
generateOnShellMomenta[range_:{-1,1}]:=Module[{a,b,initialMomenta,soln,otherMomenta},
	initialMomenta=Table[k[i]->generateMasslessMomentum[range],{i,1,3}];
	a[3]=RandomReal[range];
	b[4]=RandomReal[range];
soln=Quiet[Solve[
		Join[
			Table[Sum[(k[i]/.initialMomenta)[[j]],{i,3}]+a[j]+b[j]==0,{j,4}],
			{a[1]^2-Sum[a[i]^2,{i,2,4}]==0,b[1]^2-Sum[b[i]^2,{i,2,4}]==0}
		],
		Join[Drop[Table[a[i],{i,4}],{3}],Table[b[i],{i,3}]],
		Reals
	]];
	otherMomenta={k[4]->Table[a[i],{i,4}],k[5]->Table[b[i],{i,4}]}/.soln[[1]];
	Return[Join[initialMomenta,otherMomenta]]
];
minkowskiMetric=DiagonalMatrix[{1,-1,-1,-1}];
performMomentaDots={dot[x_k,y_k]:>x.minkowskiMetric.y};
checkMomentumPoint[]:={
	Table[(Simplify[dot[k[i],k[i]]/.performMomentaDots/.momenta]),{i,1,Length[momenta]}],
	Simplify[Sum[k[i],{i,1,Length[momenta]}]/.momenta],
	Table[Simplify[dot[k[i],k[i]]/.performMomentaDots/.referenceMomenta/.momenta],{i,13,15}]
};
replaceSpinorProducts={
	(f:(spA|spS))[i_,j_]:>f[k[i],k[j]]
};
(*Real momenta, k in lcc: (-,+,perp,perp-bar). Use only with explicit momenta*)
kl[x_]:=Join[Table[x[[1]]+i x[[4]],{i,{-1,+1}}],Table[x[[2]]+i I x[[3]],{i,{+1,-1}}]];
performSpinorProducts={
	spA[x_,y_]:> Sqrt[kl[y][[2]]]/Sqrt[kl[x][[2]]]kl[x][[3]]-Sqrt[kl[x][[2]]]/Sqrt[kl[y][[2]]]kl[y][[3]],
	spS[x_,y_]:>-Sqrt[kl[y][[2]]]/Sqrt[kl[x][[2]]]kl[x][[4]]+Sqrt[kl[x][[2]]]/Sqrt[kl[y][[2]]]kl[y][[4]]
};
square1={s[i_,j_]:>spA[i,j] spS[j,i]};
square2={s[i_,j_]:>2*dot[k[i],k[j]]};
antisymmetry={(f:(spA|spS))[i_,j_]:>Signature[{i,j}]*f@@Sort[{i,j}]};


(*If this fails, just run it again*)
momenta=generateOnShellMomenta[];


referenceMomenta={k[13]->k[2],k[14]->k[1],k[15]->k[1]};


referenceMomenta=Table[k[12+i]->generateMasslessMomentum[],{i,3}];


momenta//TableForm
referenceMomenta//TableForm
(*These should all be zero*)
Chop[checkMomentumPoint[]]//TableForm


SetAttributes[dot,Orderless];
quarkQuarkGluonVertex[i_,{\[Alpha]_,\[Beta]_}]:=(I/Sqrt[2])*dot[gamma[m[i]],{s[\[Alpha]],s[\[Beta]]}];
gluonPropagator[i_]:=-I/(dot[k[i],k[i]]);
quarkPropagator[i_,{\[Alpha]_,\[Beta]_},sign_:1]:=Module[{j},
	sign*I*dot[gamma[m[j]],{s[\[Alpha]],s[\[Beta]]}]*dot[k[i],m[j]]/dot[k[i],k[i]]
];
fourGluonVertex[a_,b_,c_,d_]:=I(
	dot[m[a],m[c]]*dot[m[d],m[b]]-(1/2)*(dot[m[a],m[d]]*dot[m[c],m[b]]+dot[m[a],m[b]]*dot[m[d],m[c]])
);
threeGluonVertex[a_,b_,c_,aSign_:1,bSign_:1,cSign_:1]:=(I/Sqrt[2])*(
	dot[m[c],m[a]]*(cSign dot[k[c],m[b]]- aSign dot[k[a],m[b]])+
	dot[m[a],m[b]]*(aSign dot[k[a],m[c]]- bSign dot[k[b],m[c]])+
	dot[m[b],m[c]]*(bSign dot[k[b],m[a]]- cSign dot[k[c],m[a]])
);
(*All momenta out; off-shell momenta to right*)
graphA[a_,b_,c_,d_,e_]:=Module[{f,g},
	quarkQuarkGluonVertex[f,{b,a}]*gluonPropagator[f]*threeGluonVertex[f,c,g]*gluonPropagator[g]*threeGluonVertex[g,d,e,-1,1,1]//.
	{
		dot[k[f],x_]:>dot[k[a],x]+dot[k[b],x],
		dot[k[g],x_]:>dot[k[d],x]+dot[k[e],x]
	}
];
graphB[a_,b_,c_,d_,e_]:=Module[{f,g},
	quarkQuarkGluonVertex[f,{b,a}]*gluonPropagator[f]*threeGluonVertex[f,g,e]*gluonPropagator[g]*threeGluonVertex[g,c,d,-1,1,1]//.
	{
		dot[k[f],x_]:>dot[k[a],x]+dot[k[b],x],
		dot[k[g],x_]:>dot[k[c],x]+dot[k[d],x]
	}
];
graphC[a_,b_,c_,d_,e_]:=Module[{f},
	quarkQuarkGluonVertex[f,{b,a}]*gluonPropagator[f]*fourGluonVertex[f,c,d,e]//.
	{
		dot[k[f],x_]:>dot[k[a],x]+dot[k[b],x]
	}
];
graphD[a_,b_,c_,d_,e_]:=Module[{f,f1,g},
	threeGluonVertex[g,c,d,-1,1,1]*gluonPropagator[g]*quarkQuarkGluonVertex[g,{b,f}]*quarkPropagator[f,{f,f1},-1]*quarkQuarkGluonVertex[e,{f1,a}]//.
	{
		dot[k[f],x_]:>dot[k[a],x]+dot[k[e],x],
		dot[k[g],x_]:>dot[k[c],x]+dot[k[d],x]
	}
];
graphE[a_,b_,c_,d_,e_]:=Module[{f,g,g1},
	quarkQuarkGluonVertex[c,{b,g}]*quarkPropagator[g,{g,g1}]*quarkQuarkGluonVertex[f,{g1,a}]*gluonPropagator[f]*threeGluonVertex[f,d,e,-1,1,1]//.
	{
		dot[k[f],x_]:>dot[k[d],x]+dot[k[e],x],
		dot[k[g],x_]:>dot[k[b],x]+dot[k[c],x]
	}
];
graphF[a_,b_,c_,d_,e_]:=Module[{f,g,f1,g1},
	quarkQuarkGluonVertex[c,{b,g}]*quarkPropagator[g,{g,g1}]*quarkQuarkGluonVertex[d,{g1,f}]*quarkPropagator[f,{f,f1},-1]*quarkQuarkGluonVertex[e,{f1,a}]//.
	{
		dot[k[f],x_]:>dot[k[a],x]+dot[k[e],x],
		dot[k[g],x_]:>dot[k[b],x]+dot[k[c],x]
	}
];
aNOrdered[chiralities_]:=dot[lS[2],s[2]]*dot[rA[1],s[1]]*Product[dot[epsilon[chiralities[[i]],k[i],k[10+i]],m[i]],{i,3,Length[chiralities]}];
V=graphA[1,2,3,4,5]+graphB[1,2,3,4,5]+graphC[1,2,3,4,5]+graphD[1,2,3,4,5]+graphE[1,2,3,4,5]+graphF[1,2,3,4,5];
amplitudeOnce[expr_]:=Module[{tmp},
	tmp = expr /. {
		dot[x_k,y_m]*dot[y_m,z_k]:>dot[x,z],
		dot[x_m,y_m]*dot[y_m,z_]:>dot[x,z],
		dot[gamma[x_m],{a_s,b_s}]*dot[y_m,x_m]:>dot[gamma[y],{a,b}],
		dot[epsilon[L,k[i_],k[j_]],m_m]:>-spSA[j,gamma[m],i]/(Sqrt[2]*spS[j,i]),
		dot[epsilon[R,k[i_],k[j_]],m_m]:> spAS[j,gamma[m],i]/(Sqrt[2]*spA[j,i]),
		dot[epsilon[p_Integer,k[i_],k[j_]],m_m]:>spAS[p,gamma[m],p],
		spAS[j_,x_gamma,i_]:>spSA[i,x,j],
		spSA[i_,x_gamma,j_]*spSA[k_,x_gamma,l_]:>2*spS[i,k]*spA[l,j],
		dot[x_m,y_m]*spSA[i_,gamma[y_m],j_]:>spSA[i,gamma[x],j],
		dot[k[i_],x_m]*spSA[j_,gamma[x_m],l_]:>spS[j,i]spA[i,l],
		(f:(spA|spS))[i_,i_]:>0,
		dot[x_k,x_k]:>0,
		dot[lS[i_],a_s]*dot[k[y_],x_m]*dot[gamma[x_m],{a_s,b_s}]*dot[rA[j_],b_s]:>spS[i,y]spA[y,j],
		dot[gamma[x_m],{a_s,b_s}]*dot[k[i_],x_m]:>dot[rA[i],a]dot[lS[i],b]+dot[rS[i],a]dot[lA[i],b],
		dot[gamma[x_m],{a_s,b_s}]spSA[i_,gamma[x_m],j_]:>2*(dot[rS[i],a]*dot[lA[j],b]+dot[rA[j],a]*dot[lS[i],b]),
		dot[lS[i_],a_s]*dot[rA[j_],a_s]:>0,
		dot[lA[i_],a_s]*dot[rS[j_],a_s]:>0,
		dot[lA[i_],a_s]*dot[rA[j_],a_s]:>spA[i,j],
		dot[lS[i_],a_s]*dot[rS[j_],a_s]:>spS[i,j],
		(f:(spA|spB))[i_,j_]:>Signature[{i,j}]*f@@Sort[{i,j}]
	};
	Return[Expand[tmp]];
];
amplitude[expr_]:=FixedPoint[amplitudeOnce,expr];
(*Note that the first two chiralities are currently hard coded as {L, R} regardless of what we put in the chiralities vector here*)
answer=amplitude[aNOrdered[{L,R,L,R,R}]V];
Chop[Simplify[answer/.performMomentaDots/.replaceSpinorProducts/.referenceMomenta/.momenta/.performSpinorProducts]]


-I spA[1,3]^3 / (spA[1,2] spA[3,4] spA[4,5] spA[5,1])/.square1/.antisymmetry/.replaceSpinorProducts/.momenta/.performSpinorProducts
