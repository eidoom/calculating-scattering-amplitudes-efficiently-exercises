(* ::Package:: *)

azimuthalRange={0,2\[Pi]};
polarRange={0,\[Pi]};
phi1=RandomReal[azimuthalRange];
theta1=RandomReal[polarRange];
phi00=RandomReal[azimuthalRange];
theta00=RandomReal[polarRange];
s1=RandomInteger[{1,10}];


phi2=RandomReal[azimuthalRange];
theta2=RandomReal[polarRange];
phi3=RandomReal[azimuthalRange];
theta3=RandomReal[polarRange];
phi4=RandomReal[azimuthalRange];
theta4=RandomReal[polarRange];
phi5=RandomReal[azimuthalRange];
theta5=RandomReal[polarRange];
referenceMomenta={
	k[11]->Sqrt[s]{1,Sin[phi]Cos[theta],Sin[phi]Sin[theta],Cos[phi]}/.{theta->theta2,phi->phi2,s->s1},
	k[12]->Sqrt[s]{1,Sin[phi]Cos[theta],Sin[phi]Sin[theta],Cos[phi]}/.{theta->theta3,phi->phi3,s->s1},
	k[13]->Sqrt[s]{1,Sin[phi]Cos[theta],Sin[phi]Sin[theta],Cos[phi]}/.{theta->theta4,phi->phi4,s->s1},
	k[14]->Sqrt[s]{1,Sin[phi]Cos[theta],Sin[phi]Sin[theta],Cos[phi]}/.{theta->theta5,phi->phi5,s->s1}
};


momenta={
	k[1]->-Sqrt[s]{1,Sin[phi0]Cos[theta0],Sin[phi0]Sin[theta0],Cos[phi0]},
	k[2]->-Sqrt[s]{1,-Sin[phi0]Cos[theta0],-Sin[phi0]Sin[theta0],-Cos[phi0]},
	k[3]->Sqrt[s]{1,Sin[phi]Cos[theta],Sin[phi]Sin[theta],Cos[phi]},
	k[4]->Sqrt[s]{1,-Sin[phi]Cos[theta],-Sin[phi]Sin[theta],-Cos[phi]}
}/.{theta->theta1,phi->phi1,theta0->theta00,phi0->phi00,s->s1};
(*referenceMomenta={k[11]\[Rule]k[4],k[12]\[Rule]k[4],k[13]\[Rule]k[1],k[14]\[Rule]k[1]};*)
minkowskiMetric=DiagonalMatrix[{1,-1,-1,-1}];
performMomentaDots={dot[x_k,y_k]:>x.minkowskiMetric.y};
checkMomentumPoint={
	Table[(Simplify[dot[k[i],k[i]]/.performMomentaDots/.momenta]),{i,1,4}],
	Simplify[Sum[k[i],{i,1,4}]/.momenta],
	Table[Simplify[dot[k[i],k[i]]/.performMomentaDots/.referenceMomenta/.momenta],{i,11,14}]
};
replaceSpinorProducts={
	(f:(spA|spS))[i_,j_]:>f[k[i],k[j]]
};
(*Real momenta, k in lcc: (-,+,perp,perp-bar). Use only with explicit momenta*)
Chop[checkMomentumPoint]==Table[Table[0.,{i,4}],{i,3}]
kl[x_]:=Join[Table[x[[1]]+i x[[4]],{i,{-1,+1}}],Table[x[[2]]+i I x[[3]],{i,{+1,-1}}]];
performSpinorProducts={
	spA[x_,y_]:> Sqrt[kl[y][[2]]]/Sqrt[kl[x][[2]]]kl[x][[3]]-Sqrt[kl[x][[2]]]/Sqrt[kl[y][[2]]]kl[y][[3]],
	spS[x_,y_]:>-Sqrt[kl[y][[2]]]/Sqrt[kl[x][[2]]]kl[x][[4]]+Sqrt[kl[x][[2]]]/Sqrt[kl[y][[2]]]kl[y][[4]]
};


referenceMomenta={k[11]->k[4],k[12]->k[4],k[13]->k[1],k[14]->k[1]};


SetAttributes[dot,Orderless];
fourGluonVertex[a_,b_,c_,d_]:=I(
	dot[m[a],m[c]]*dot[m[d],m[b]]-(1/2)*(dot[m[a],m[d]]*dot[m[c],m[b]]+dot[m[a],m[b]]*dot[m[d],m[c]])
);
threeGluonVertex[a_,b_,c_,aSign_:1,bSign_:1,cSign_:1]:=(I/Sqrt[2])*(
	dot[m[c],m[a]]*(cSign dot[k[c],m[b]]- aSign dot[k[a],m[b]])+
	dot[m[a],m[b]]*(aSign dot[k[a],m[c]]- bSign dot[k[b],m[c]])+
	dot[m[b],m[c]]*(bSign dot[k[b],m[a]]- cSign dot[k[c],m[a]])
);
(*All momenta out; off-shell momenta to right*)
doubleThreeGluonGraph[a_,b_,c_,d_]:=Module[{e},(-I/dot[k[e],k[e]])*threeGluonVertex[a,e,b]*threeGluonVertex[c,e,d,1,-1,1]//.{dot[k[e],x_]:>dot[k[c],x]+dot[k[d],x]}];
V=fourGluonVertex[1,2,3,4]+doubleThreeGluonGraph[1,2,3,4]+doubleThreeGluonGraph[2,3,4,1];
aNOrdered[chiralities_]:=Product[epsilon[chiralities[[i]],m[i],k[i],k[10+i]],{i,1,Length[chiralities]}];
amplitude[expr_]:=Expand[expr]//.{
	dot[x_,y_m]*dot[y_m,z_]:>dot[x,z],
	epsilon[l_,x_m,p_k,q_k]*dot[x_m,y_m]:>epsilon[l,y,p,q],
	epsilon[L,m_m,k[i_],k[j_]]:>-spSA[j,gamma[m],i]/(Sqrt[2]*spS[j,i]),
	epsilon[R,m_m,k[i_],k[j_]]:> spAS[j,gamma[m],i]/(Sqrt[2]*spA[j,i]),
	epsilon[p_Integer,m_m,k[i_],k[j_]]:>spAS[p,gamma[m],p],
	spAS[j_,x_gamma,i_]:>spSA[i,x,j],
	spSA[i_,x_gamma,j_]*spSA[k_,x_gamma,l_]:>2*spS[i,k]*spA[l,j],
	dot[k[i_],x_m]*spSA[j_,gamma[x_m],l_]:>spS[j,i]spA[i,l],
	(f:(spA|spS))[i_,i_]:>0,
	dot[x_k,x_k]:>0(*,
	(f:(spA|spS))[i_,j_]\[RuleDelayed]Signature[{i,j}]*f@@Sort[{i,j}]*)
};
ch={{L,L,L,L},{R,L,L,L},{L,L,R,R},{L,R,L,R},{R,R,R,R},{L,R,R,R},{R,R,L,L},{R,L,R,L}};
answer=amplitude[aNOrdered[ch[[3]]]V];
Chop[Simplify[answer/.performMomentaDots/.replaceSpinorProducts/.referenceMomenta/.momenta/.performSpinorProducts]]


(*3*)
I spA[1,2]^4/(spA[1,2]spA[2,3]spA[3,4]spA[4,1])/.replaceSpinorProducts/.momenta/.performSpinorProducts


(*7*)
I spS[1,2]^4/(spS[1,2]spS[2,3]spS[3,4]spS[4,1])/.replaceSpinorProducts/.momenta/.performSpinorProducts


(*4*)
I spA[1,3]^4/(spA[1,2]spA[2,3]spA[3,4]spA[4,1])/.replaceSpinorProducts/.momenta/.performSpinorProducts
